import pcbnew
import os

class Localise3DModelsPluginAction(pcbnew.ActionPlugin):

    def defaults(self):
        self.name = "Localise 3D Models"
        self.category = "3D Modelling"
        self.description = "Makes 3D models local to the project for standalone distribution",
        self.icon_file_name = os.path.join(os.path.dirname(__file__), 'resources', 'icon.png')

    def Run(self):
        # The entry function of the plugin that is executed on user action
        print("Hello World")
